import React, {useContext} from 'react';
import UserContext from '../UserContext';

// import Navbar from 'react-bootstrap/Navbar'
// import Nav from 'react-bootstrap/Nav'

import { Link } from 'react-router-dom';
import {Navbar, Nav} from 'react-bootstrap';




export default function AppNavbar(){

	const {user} = useContext(UserContext);
	// const [user, setUser] = useState(localStorage.getItem("email"));
	// console.log(user);

	return (
			<Navbar bg="dark" expand="lg" variant="dark">
				<Navbar.Brand className="ms-2" href="#">Zuitt Booking</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse  id="basic-navbar-nav">
					<Nav className="ms-auto">
						<Nav.Link as={ Link } to='/'>Home</Nav.Link>
						<Nav.Link as={ Link } to='/courses'>Courses</Nav.Link>

						{ (user.accessToken !== null) ?

							<Nav.Link as={ Link } to='/logout'>Logout</Nav.Link>
							:
							<>
								<Nav.Link as={ Link } to='/register'>Register</Nav.Link>
								<Nav.Link as={ Link } to='/login'>Login</Nav.Link>
							</>
						}

					</Nav>
				</Navbar.Collapse>
			</Navbar>
		)
}