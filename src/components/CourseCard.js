import React from 'react';
import	{Row, Col, Card} from "react-bootstrap";
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProp}){

	const { _id, name, description, price} = courseProp;
	// const [count, setCount] = useState(0);
	// const [seats,setSeats] = useState(30);
	// const [isOpen, setIsOpen] = useState(true);

	// const enroll = () =>{	
	// 		setSeats(seats - 1);
	// 		setCount(count + 1)	
	// }

	// useEffect(()=> {
	// 	if (seats ===0){
	// 		setIsOpen(false);
	// 	}
	// }, [seats])

	return (
		<Row className="pt-3">
			<Col xs={12}>
				<Card className="courseCard p-3">
					<Card.Body>
						<Card.Title><h3>{ name }</h3></Card.Title>			
						<Card.Subtitle><h5>Description</h5></Card.Subtitle>
						<Card.Text>{ description }</Card.Text>
						<Card.Subtitle><h5>Price:</h5></Card.Subtitle>
						<Card.Text>&#8369; { price }</Card.Text>{/*
						<Card.Text>Enrollees: { count }</Card.Text>
						<Card.Text>Available Seats: { 30 - count }</Card.Text>*/}
						<Link className="btn btn-primary" to={`/courses/${_id}`}> View Course</Link>

					</Card.Body>

				</Card>
			</Col>
		</Row>
	)

}

CourseCard.propTypes = {
	courseProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
