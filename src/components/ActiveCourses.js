import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveCourse({course, isActive, fetchData}) {

	const archiveToggle = (courseId) => {
			
			fetch(`https://mysterious-taiga-31794.herokuapp.com/courses/${courseId}/archive`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					'Accept': 'application/json'
				}
			})
			.then(data => {
					Swal.fire({
						title: 'Success',
						icon: 'success',
						text: 'Course successfully disabled'
					})
					fetchData()
			})

	}

	const reactivate = (courseId) => {
			fetch(`https://mysterious-taiga-31794.herokuapp.com/courses/${courseId}/reactivate`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					 'Accept': 'application/json'
				}
			})
			.then(data => {

					Swal.fire({
						title: 'Success',
						icon: 'success',
						text: 'Course successfully reactivated'
					})
					fetchData()
			})
		}	
	return(
		<>
			{isActive ?

				<Button variant="danger" size="sm" onClick={() => archiveToggle(course)}>Archive</Button>

				:

				<Button variant="success" size="sm" onClick={() => reactivate(course)}>Unarchive</Button>
			}
		</>

		)
}