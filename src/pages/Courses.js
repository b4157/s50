import React, {useState, useEffect, useContext} from 'react';

// import	coursesData from "../mockData/coursesData";
// import CourseCard from '../components/CourseCard';
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';
import UserContext from '../UserContext';

export default function Courses(){
	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} courseProp={course}/>
	// 		)
	// })
	const { user } = useContext (UserContext)
	const [allCourses, setAllCourses] = useState([]);

	const fetchData = () => {
		fetch('https://mysterious-taiga-31794.herokuapp.com/courses/all')
		.then(res=> res.json())
		.then (data => {
			setAllCourses(data)
		})
	}

	useEffect(()=> {
		fetchData()
	}, [])

	return (
		<>

			{ (user.isAdmin===true)?

				<AdminView coursesData={allCourses} fetchData={fetchData}/>

				:

				<UserView coursesData={allCourses}/>
			}

		


		</>
	)

}