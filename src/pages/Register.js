import React, {useState, useEffect, useContext} from 'react';
import	{Form, Button} from "react-bootstrap";
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';

export default function Register(){

	const {user, setUser} = useContext(UserContext);
	const navigate = useNavigate();

	// store values of input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(true);
	const [lastName, setLastName] = useState('');
	const [firstName, setFirstName] = useState('');
	const [mobileNum, setMobileNum] = useState('');
	const [sex, setSex] = useState('');

	useEffect(()=>{
		if(email !=='' && password1 !== '' && password2 !=='' && lastName!=='' && firstName !=='' && mobileNum!=='' && sex!==''){
			if (password1 !== password2) {
				Swal.fire({
				  title: 'Password Mismatch',
				  text: 'Your password did not match. Try again!',
				  icon:'error'
				})
				setIsActive(true);
			}
			

		} else{
			setIsActive(false);
		}
	}, [email, password1, password2, isActive, lastName,firstName, mobileNum,sex])

	
	function registerUser(e){
		e.preventDefault();


		fetch('https://mysterious-taiga-31794.herokuapp.com/users/register', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				firstName: firstName,
			    lastName: lastName,
			    email: email,
			    password: password1,
			    gender: sex,
			    mobileNo: mobileNum
			})
		})
		.then(response => response.json())
		.then(data => {		
			Swal.fire({
			  title: 'Good job!',
			  text: 'Registration successful! Login Now!',
			  icon:'success'
			})
			setEmail('');
			setPassword1('');
			setPassword2('');
			setSex('');
			setMobileNum('');
			setFirstName('');
			setLastName ('');
			navigate('/login')
		})	

		
	}

	return (

		(user.accessToken !== null) ?

			<Navigate to="/" />
		:

			<Form className = "p-2" onSubmit={(e) => registerUser(e) }>
				<Form.Group>
					<h1 className = "text-center mb-2">Register</h1>
					<Form.Label>Email Address</Form.Label>
					<Form.Control 
						type="email"
						placeholder = "ex.: juan@email.com"
						required
						value={email}
						onChange={e => setEmail(e.target.value)}
					/>
					<Form.Text className = "text-muted">
						We will never share your email with anyone else.
					</Form.Text>
				</Form.Group>

				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control 
						type="password"
						placeholder = "Enter password"
						required
						value={password1}
						onChange={e => setPassword1(e.target.value)}
					/>
					<Form.Text className = "text-muted">
						User combination of lowercase and uppercase texts, special characters and numbers.
					</Form.Text>
				</Form.Group>

				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control 
						type="password"
						placeholder = "Verify password"
						required
						value={password2}
						onChange={e => setPassword2(e.target.value)}
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>First Name</Form.Label>
					<Form.Control 
						placeholder = "ex: Juan"
						required
						value={firstName}
						onChange={e => setFirstName(e.target.value)}
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Last Name</Form.Label>
					<Form.Control 
						placeholder = "ex: Dela Cruz"
						required
						value={lastName}
						onChange={e => setLastName(e.target.value)}
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Sex</Form.Label>
					<Form.Control
						as="select"	 
						required
						value={sex}
						onChange={e => setSex(e.target.value)}
					>		
					  <option>Select here</option>
					  <option value="Male">Male</option>
					  <option value="Female">Female</option>
					</Form.Control>
					
				</Form.Group>

				<Form.Group>
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control 
						placeholder = "0917 1123 3289"
						required
						value={mobileNum}
						onChange={e => setMobileNum(e.target.value)}
					/>
				</Form.Group>

				

				{isActive ?
					<Button className="mt-3" variant="primary" type="submit" > Submit </Button>
					:
					<Button className="mt-3" variant="primary" type="submit" disabled> Submit </Button>
				}

			</Form>
	)
}